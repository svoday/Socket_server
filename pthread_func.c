/*************************************************************************
    > File Name: pthread_func.c
    > Author: JiaMing
    > Mail: day961@gmail.com 
    > Created Time: 2014年10月24日 星期五 11时28分39秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include "tcp_funcinit.h"
#include "global_def.h"
#include "pthread_func.h"
#include "pthread_pool.h"

#define MAXBUF 128
#define MAXLINK	5

int fd_A[MAXLINK];  // �ļ�����������

void *server_thread()
{
    int sock_fd0, linked_fd;
	int ret, c_len = 0;
	char buf[MAXBUF];
	struct sockaddr_in client_addr;
    
    ret = socket_init(&sock_fd0);
    if(ret < 0){  
		perror("Socket Init Error:");  
		exit(1);
	}
    
	ret = listen(sock_fd0, MAXLINK);
	if(ret < 0){  
		perror("listen error:");  
		exit(1);  
	} 
	
	fd_set fd_SetRead;
    fd_set fd_SetWrite;
	struct timeval tv;
    int conn_amount = 0;    // ��������
    int i;
    FileInfo client_fileinfo;

	while(1){
		FD_ZERO(&fd_SetRead);
        FD_ZERO(&fd_SetWrite);
		FD_SET(sock_fd0, &fd_SetRead);
        FD_SET(sock_fd0, &fd_SetWrite);
		tv.tv_sec = 30;
		tv.tv_usec = 0;
		for(i = 0; i < MAXBUF; i++){
			if(fd_A[i] != 0){
				FD_SET(fd_A[i], &fd_SetRead);
			}
		}
        for(i = 0; i < MAXBUF; i++){
			if(fd_A[i] != 0){
				FD_SET(fd_A[i], &fd_SetWrite);
			}
		}
        
        ret = select(MAXLINK + 1, &fd_SetRead, &fd_SetWrite, NULL, &tv);
        if(ret < 0){
            perror("select error!");
            break;
        }else if (ret == 0){
            printf("timeout\n");
            continue;
        }
        
        /*exise connections handle*/
        for (i = 0; i < conn_amount; i++){
            if (FD_ISSET(fd_A[i], &fd_SetRead)){
                memset(buf, 0, MAXBUF);
                ret = recv(fd_A[i], (char *)&client_fileinfo, sizeof(FileInfo), 0);
                if (ret <= 0){      //�������ݳ����������ѶϿ�
                    printf("client[%d] close\n", i);
                    close(fd_A[i]);
                    FD_CLR(fd_A[i], &fd_SetRead);
                    fd_A[i] = 0;
                    conn_amount--;
                }else{              // ���ݽ��ճɹ�
//                    printf("client[%d] send:%s\n", i, buf);
                
                    printf("len:%lu, filename:%s\n", client_fileinfo.FileLen, client_fileinfo.FileName);
                }
            }
        }
        
        /*A new connection come*/
        if (FD_ISSET(sock_fd0, &fd_SetRead)) {            
            linked_fd = accept(sock_fd0, (struct sockaddr *)&client_addr, &c_len);
            if (linked_fd <= 0){
                perror("accept socket error!");
                continue;
            }
            // ���µ����Ӽ��뵽����ļ�����������
            if (conn_amount < MAXLINK) {
                fd_A[conn_amount] = linked_fd;
                printf("new connection client[%d] %s:%d\n", conn_amount,
                    inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
                conn_amount++;
            }else{
                printf("max connections arrive, ignore\n");
//                send(linked_fd, "bye", 4, 0);
                close(linked_fd);
                continue;
            }
        }
	}
}

void *myprocess (void *arg)
{
    printf ("threadid is 0x%x, working on task %d\n", pthread_self (), *(int *) arg);
    sleep(1);
    return NULL;
}


