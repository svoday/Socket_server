/*************************************************************************
    > File Name: tcp_funcinit.c
    > Author: JiaMing
    > Mail: day961@gmail.com 
    > Created Time: 2014年10月23日 星期四 17时06分59秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "tcp_funcinit.h"


int socket_init(int *sockfd)
{
    int ret, c_len = 0;
	struct sockaddr_in server_addr;
    
    *sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(*sockfd < 0){  
		perror("socket error:");  
		return -1;
	}
    
    memset(&server_addr, 0, sizeof(server_addr));  
	server_addr.sin_family = AF_INET;  
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	server_addr.sin_port = htons(DEFAULT_PORT);  

	ret = bind(*sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if(ret < 0){  
		perror("bind error");  
		return -1;
	} 
    
    return 0;
}

