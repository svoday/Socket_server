/***********************************************
phtread_pool.h
************************************************/

#ifndef __PTHREAD_POOL_HEAD
#define __PTHREAD_POOL_HEAD
/*
* 任务结构体
*线程池里所有运行和等待的任务都是一个CThread_worker
*由于所有任务都在链表里，所以是一个链表结构
*/
typedef struct worker
{
    void *(*process) (void *arg);   //回调函数，任务运行时会调用此函数，注意也可声明成其它形式
    void *arg;                      //回调函数的参数*/
    struct worker *next;
} CThread_worker;

/*线程池结构*/
typedef struct
{
    pthread_mutex_t queue_lock;
    pthread_cond_t queue_ready;//线程条件变量

    CThread_worker *queue_head;//链表结构，线程池中所有等待任务*/

    /*是否销毁线程池*/
    int shutdown;
    /*线程标示符*/
    pthread_t *threadid;
    /*线程池中允许的活动线程数目*/
    int max_thread_num;
    /*当前等待队列的任务数目*/
    int cur_queue_size;

} CThread_pool;

void tpool_thread_init (int max_thread_num);
int tpool_add_worker (void *(*process) (void *arg), void *arg);
int tpool_thread_destroy ();

static void *tpool_thread_routine (void *arg);

#endif
