/*************************************************************************
    > File Name: tcp_server.c
    > Author: JiaMing
    > Mail: day961@gmail.com 
    > Created Time: 2014年09月25日 星期四 15时40分22秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include "tcp_funcinit.h"
#include "global_def.h"
#include "pthread_func.h"

#define MAXBUF 128
#define MAXLINK	5

int fd_A[MAXLINK];  // 文件描述符集合

int main(int argc, char *argv[])
{
    pthread_t server_thread_t;
    
    //创建线程
	if(pthread_create(&server_thread_t, NULL, server_thread, NULL) != 0){  
		perror("Pthread Create Error");
		exit(1);  
	}

    while(1){
    }

	return 0;
}
/***************************************** 
 * * 函数名称：rec_data 
 * * 功能描述：接受客户端的数据 
 * * 参数列表：fd——连接套接字 
 * * 返回结果：void 
 * *****************************************/  
void *rec_data(void *fd)  
{  
	int client_sockfd;  
	int i,byte;  
	char char_recv[100];//存放数据  
	client_sockfd=*((int*)fd);  
	for(;;)  
    {
	if((byte=recv(client_sockfd,char_recv,100,0))==-1){  
		perror("recv");  
		exit(EXIT_FAILURE);   
	}  
	if(strcmp(char_recv, "exit")==0)//接受到exit时，跳出循环  
		break;  
		printf("receive from client is %s/n",char_recv);//打印收到的数据  
	}  
	free(fd);  
	close(client_sockfd);  
	pthread_exit(NULL);  
}



void filefunc()
{
    FILE * fp = fopen("a","r");
}

