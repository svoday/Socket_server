/*************************************************************************
    > File Name: global_def.h
    > Author: JiaMing
    > Mail: day961@gmail.com 
    > Created Time: 2014年10月24日 星期五 09时11分34秒
 ************************************************************************/

#ifndef __GLOBAL_HEAD
#define __GLOBAL_HEAD

#define PACK_BUFF       1024
#define FILE_NAME_MAX   20

//�ļ���Ϣ
typedef struct _FileInfo{
    u_long FileLen;                 //�ļ�����
    char FileName[FILE_NAME_MAX];   //�ļ���������
}FileInfo;

//���ݰ�
typedef struct _DataPack{
    char cType;                     //'D'Ϊ����  'M'Ϊ�ļ���Ϣ
    int PackLen;
    char Content[PACK_BUFF];        //���ݰ�������
    u_long Position;                //�������ļ��е�λ��
    int ContentLen;                 //�����ֽ���
    FileInfo FileInfor;            //�ļ���Ϣ
}DataPack;

#endif

